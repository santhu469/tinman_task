const CustomersController = require("../controllers/customers_controller.js");
const CooksController = require('../controllers/cooks_controller.js');
const MenuItemsController = require('../controllers/cooks_controller.js');
const OrdersController = require('../controllers/orders_controller.js');
const RunnersController = require('../controllers/runners_controller.js');

module.exports = app => {

    app.route("/ping").get( (req, resp)=>{
        resp.send({
            message:"pong"
        })
    })

    // customers
    app.route("/customer/create").post(CustomersController.create);
    app.route("/all_customers").get(CustomersController.getAll);
    app.route("/customer/update").post(CustomersController.update);
    app.route("/customer/delete").delete(CustomersController.delete);

    // cooks
    app.route("/cook/create").post(CooksController.create);
    app.route("/all_cooks").get(CooksController.getAll);
    app.route("/cook/update").post(CooksController.update);
    app.route("/cook/delete").delete(CooksController.delete);

    // menu items
    app.route("/item/create").post(MenuItemsController.create);
    app.route("/all_items").get(MenuItemsController.getAll);
    app.route("/item/update").post(MenuItemsController.update);
    app.route("/item/delete").delete(MenuItemsController.delete);

    // orders 
    app.route("/order/create").post(OrdersController.create);
    app.route("/all_orders").get(OrdersController.getAll);
    app.route("/order/update").post(OrdersController.update);
    app.route("/order/delete").delete(OrdersController.delete);

    // runners 
    app.route("/order/create").post(RunnersController.create);
    app.route("/all_orders").get(RunnersController.getAll);
    app.route("/order/update").post(RunnersController.update);
    app.route("/order/delete").delete(RunnersController.delete);
}