const mongoose = require('mongoose');

let cooks = new mongoose.Schema({
    "name" : String,
    "email" : String,
    "phone" : { type:Number, required:true },
    "createdAt": { type: Date, default: Date.now },
    "description": String,
    "address" : String,
    "location" : String,
    "open_status": Boolean,
    "ratings": String
});

const CooksModel = mongoose.model("cooks", cooks)

module.exports = CooksModel;