const mongoose = require('mongoose');

let menuItems = new mongoose.Schema({
    "id":Number,
    "item_name" : String,
    "removed_status": Boolean,
    "price" : String,
    "availability" : Boolean,
    "description": String,
    "rating" : Number
});

const MenuItemsModel = mongoose.model("menuItems", menuItems)

module.exports = MenuItemsModel;