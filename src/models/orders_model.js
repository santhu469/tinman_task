const mongoose = require('mongoose');

let orders = new mongoose.Schema({
    "id":Number,
    "order_name" : String,
    "from": String,
    "quantity" : Number,
    "date_of_order" : Date,
    "date_of_deleviery_order" : Date,
    "description": String,
    "rating" : Number
});

const OrdersModel = mongoose.model("orders", orders)

module.exports = OrdersModel;