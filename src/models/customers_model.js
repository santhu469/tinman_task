const mongoose = require('mongoose');

let customer = new mongoose.Schema({
    "name" : {
    	type:String, 
    	required:true
    },
    "id" : {
    	type:Number
    },
    "email"	: {
		type: String,
		required: true,
		unique: true,
		lowercase: true
	},
    "phone" : {
		type:String, 
		required:true
	},
    "updatedAt" : { 
		type: Date, 
		default: Date.now 
	},
    "createdAt" : { 
		type: Date, 
		default: Date.now 
	},
    "age" : Number,
    "experience" : Number,
    "description" : String,
    "address" : String
});

const CustomerModel = mongoose.model("customer", customer)

module.exports = CustomerModel;