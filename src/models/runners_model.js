const mongoose = require('mongoose');

let runners = new mongoose.Schema({
    "name" : String,
    "email" : String,
    "phone" : Number,
    "description": String,
    "address" : String
});

const RunnersModel = mongoose.model("runners", runners)

module.exports = RunnersModel;