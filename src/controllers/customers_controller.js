const CustomersModel = require('../models/customers_model');

module.exports = {
    create: async (req, resp) => {
        let customerData = req.body;
        CustomersModel.create(customerData, (error, customer) => {
            if (error) {
                resp.send({
                    message:"error"
                })
            }else{
                resp.send({
                    message:"success",
                    status: 200,
                    data:customer
                })
            }
        })
    },

    getAll: async (req, resp) => {
        CustomersModel.find({}, (error, customersList) =>{
            if (error) {
                resp.send({
                    message:"something went wrong"
                });
            }else{
                resp.send({
                    message:"success",
                    data:customersList,
                    status:200
                });
            }
        })
    },

    update: async (req, resp) => {
        let id = req.body;
        CustomersModel.findByIdAndUpdate({_id:id}, (error, customersList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:customersList,
                    status:200
                });
            }
        })
    },

    delete: async (req, resp) => {
        let id = req.body;
        CustomersModel.deleteOne({_id:id}, (error, customersList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:customersList,
                    status:200
                });
            }
        })
    }
}