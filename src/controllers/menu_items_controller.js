const MenuItemsModel = require('../models/menu_items_model');

module.exports = {
    create: async (req, resp) => {
        let itemData = req.body;
        MenuItemsModel.create(itemData, (error, book) => {
            if (error) {
                resp.send({
                    message:"error"
                })
            }else{
                resp.send({
                    message:"success",
                    status: 200,
                    data:book
                })
            }
        })
    },

    getAll: async (req, resp) => {
        MenuItemsModel.find({}, (error, itemsList) =>{
            if (error) {
                resp.send({
                    message:"something went wrong"
                });
            }else{
                resp.send({
                    message:"success",
                    data:itemsList,
                    status:200
                });
            }
        })
    },

    update: async (req, resp) => {
        let id = req.body;
        MenuItemsModel.findByIdAndUpdate({_id:id}, (error, itemsList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:itemsList,
                    status:200
                });
            }
        })
    },

    delete: async (req, resp) => {
        let id = req.body;
        MenuItemsModel.deleteOne({_id:id}, (error, itemsList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:itemsList,
                    status:200
                });
            }
        })
    }
}