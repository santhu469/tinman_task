const CooksModel = require('../models/cooks_model');

module.exports = {
    create: async (req, resp) => {
        let cookData = req.body;
        CooksModel.create(cookData, (error, book) => {
            if (error) {
                resp.send({
                    message:"error"
                })
            }else{
                resp.send({
                    message:"success",
                    status: 200,
                    data:book
                })
            }
        })
    },

    getAll: async (req, resp) => {
        CooksModel.find({}, (error, cooksList) =>{
            if (error) {
                resp.send({
                    message:"something went wrong"
                });
            }else{
                resp.send({
                    message:"success",
                    data:cooksList,
                    status:200
                });
            }
        })
    },

    update: async (req, resp) => {
        let id = req.body;
        CooksModel.findByIdAndUpdate({_id:id}, (error, cooksList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:cooksList,
                    status:200
                });
            }
        })
    },

    // this will remove from the db
    // need alternate process
    delete: async (req, resp) => {
        let id = req.body;
        CooksModel.deleteOne({_id:id}, (error, cooksList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:cooksList,
                    status:200
                });
            }
        })
    }
}