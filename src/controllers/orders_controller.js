const OrdersModel = require('../models/orders_model');

module.exports = {
    create: async (req, resp) => {
        let OrderData = req.body;
        OrdersModel.create(OrderData, (error, order) => {
            if (error) {
                resp.send({
                    message:"error"
                })
            }else{
                resp.send({
                    message:"success",
                    status: 200,
                    data:order
                })
            }
        })
    },

    getAll: async (req, resp) => {
        OrdersModel.find({}, (error, orderssList) =>{
            if (error) {
                resp.send({
                    message:"something went wrong"
                });
            }else{
                resp.send({
                    message:"success",
                    data:orderssList,
                    status:200
                });
            }
        })
    },

    update: async (req, resp) => {
        let id = req.body;
        OrdersModel.findByIdAndUpdate({_id:id}, (error, orderssList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:orderssList,
                    status:200
                });
            }
        })
    },

    delete: async (req, resp) => {
        let id = req.body;
        OrdersModel.deleteOne({_id:id}, (error, orderssList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:orderssList,
                    status:200
                });
            }
        })
    }
}