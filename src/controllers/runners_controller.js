const RunnersModel = require('../models/runners_model');

module.exports = {
    create: async (req, resp) => {
        let runnerData = req.body;
        RunnersModel.create(runnerData, (error, runner) => {
            if (error) {
                resp.send({
                    message:"error"
                })
            }else{
                resp.send({
                    message:"success",
                    status: 200,
                    data:runner
                })
            }
        })
    },

    getAll: async (req, resp) => {
        RunnersModel.find({}, (error, runnerssList) =>{
            if (error) {
                resp.send({
                    message:"something went wrong"
                });
            }else{
                resp.send({
                    message:"success",
                    data:runnerssList,
                    status:200
                });
            }
        })
    },

    update: async (req, resp) => {
        let id = req.body;
        RunnersModel.findByIdAndUpdate({_id:id}, (error, runnerssList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:runnerssList,
                    status:200
                });
            }
        })
    },

    delete: async (req, resp) => {
        let id = req.body;
        RunnersModel.deleteOne({_id:id}, (error, runnerssList) =>{
            if (error) {
                resp.send(
                    {
                        message:"error"
                    }
                )
            }else{
                resp.send({
                    message:"success",
                    data:runnerssList,
                    status:200
                });
            }
        })
    }
}